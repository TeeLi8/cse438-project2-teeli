In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?


General Information:
* In playlist, an item are deleted with a long click on the item
* Recommendation button only works if the playlist is not empty
* The Search History tab is in the menu icon (top right corner)


Creative Portion:
  I added 2 extra features:

  1) Track recommendation:
     * The app can find similar tracks based on current items in the user's playlist
     * Besides the "top tracks"(which are the ones that other people like), users might
       prefer tracks that are similar to what they themselves like as recommendations
     * I used the track.getsimilar API method from Last.fm as the main resource. The app will
       make that API call and retrieve the results after a button is clicked

  2) Search History:
     * There is a new activity in the app that display all the user's search histories.
       The user can click on one of the history and the app will perform a search with with that history
       If the user want to, they can also clear the entire history list.
     * Users might want to re-perform a lot of searches of their favorite artist, and not everybody
       wants to type every time. It also might help when the users forget the name of an artist that they
       have searched before.
     * When a search is performed, the input string is saved in a SQLite database. When the user tab on the
       "Search History" tab, they will be directed to a new activity, which update its ListView with the
       history database. There is a button in the activity that clear the histories. If an item is clicked,
       the activity is going to finish and send information (a string) back to the MainActivity, in which
       the search fragment will perform a search (and update GridView) with that information.
       
  (10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(2 / 4 points) App is visually appealing
  The names of the artists on the playlist were cut off (-2)
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(15 / 15 points) Creative portion: Be creative!

The search history functionality worked great. I was not able to get the recommendations to work though. I liked your search history feature a lot so you still got full credit.

Total: 98 / 100