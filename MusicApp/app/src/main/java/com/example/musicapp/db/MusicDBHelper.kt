package com.example.musicapp.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.musicapp.models.Track

class MusicDBHelper(context: Context) : SQLiteOpenHelper(context, DBSettings.DB_NAME, null, DBSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createPlaylistQuery = " Create Table " + DBSettings.DBPLEntry.TABLE + " ( " +
                DBSettings.DBPLEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBSettings.DBPLEntry.COL_TITLE + " TEXT NULL, " +
                DBSettings.DBPLEntry.COL_IMAGE + " TEXT NULL, " +
                DBSettings.DBPLEntry.COL_ARTIST + " TEXT NULL);"

        val createHistoryQuery = " Create Table " + DBSettings.History.TABLE + " ( " +
                DBSettings.History.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBSettings.History.COL_NAME + " TEXT NULL);"

        db?.execSQL(createPlaylistQuery)
        db?.execSQL(createHistoryQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DBSettings.DBPLEntry.TABLE + "; ")
        db?.execSQL("DROP TABLE IF EXISTS " + DBSettings.History.TABLE)
        onCreate(db)
    }

    fun addTrack(track: Track){

        val db = this.writableDatabase

        val values = ContentValues().apply{
            put(DBSettings.DBPLEntry.COL_TITLE,track.title)
            put(DBSettings.DBPLEntry.COL_IMAGE,track.cURL)
            put(DBSettings.DBPLEntry.COL_ARTIST,track.artist)
        }

        val newRowId = db?.insert(DBSettings.DBPLEntry.TABLE,null,values)
    }


    fun deleteTrack(){
        val db = this.writableDatabase
        db.delete(DBSettings.DBPLEntry.TABLE,null,null)
    }

    fun addHistory(name : String){
        val db = this.writableDatabase

        val values = ContentValues().apply{
            put(DBSettings.History.COL_NAME,name)
        }

        val newRowId = db?.insert(DBSettings.History.TABLE,null,values)
    }

    fun deleteHistory(){
        val db = this.writableDatabase
        db.delete(DBSettings.History.TABLE,null,null)
    }
}