package com.example.musicapp.activities


import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.fragments.PlaylistFragment
import com.example.musicapp.models.Track
import com.example.musicapp.viewModels.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {

    private lateinit var currentTrack : Track
    private lateinit var viewModel : TrackViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)


        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)



        val currentTrackTitle = intent.getStringExtra(MainActivity.track_title)
        val currentTrackCurl = intent.getStringExtra(MainActivity.track_cURL)
        val currentTrackArtist = intent.getStringExtra(MainActivity.track_artist)
        val currentTrackPlayCount = intent.getStringExtra(MainActivity.track_playCount)
        val currentTrackListeners = intent.getStringExtra(MainActivity.track_listeners)
        currentTrack = Track(currentTrackTitle,currentTrackCurl,currentTrackArtist,currentTrackPlayCount,currentTrackListeners)

        //button
        add_button.setOnClickListener {
            viewModel.addToPlaylist(currentTrack,this)
            PlaylistFragment.shouldUpdateList = true
            Toast.makeText(this,"Add to DB!", Toast.LENGTH_LONG).show()
        }

    }

    //display info
    override fun onStart() {
        super.onStart()
        info_track_title.text = "Title: ${currentTrack.title}"
        info_track_artist.text = "Artist: ${currentTrack.artist}"
        info_track_playCount.text = "Play Count : ${currentTrack.playCount}"
        info_track_listeners.text = "Listeners: ${currentTrack.listeners}"
        Picasso.with(this).load(currentTrack.cURL).into(info_cover)
    }

}
