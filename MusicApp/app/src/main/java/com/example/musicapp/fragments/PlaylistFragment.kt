package com.example.musicapp.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import com.example.musicapp.R
import com.example.musicapp.adapters.PlaylistAdapter
import com.example.musicapp.models.Track
import com.example.musicapp.viewModels.TrackViewModel
import kotlinx.android.synthetic.main.playlist_fragment.*

@SuppressLint("ValidFragment")
class PlaylistFragment(context:Context) : Fragment(){

    private var currentPlaylist : ArrayList<Track>? = ArrayList()
    private lateinit var mAdapter: PlaylistAdapter
    private lateinit var viewModel : TrackViewModel
    private val parentContext = context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val retInf = inflater.inflate(R.layout.playlist_fragment,container,false)
        currentPlaylist = viewModel.loadPlaylist()
        if(currentPlaylist==null){
            Toast.makeText(this.context, "playlist is empty", Toast.LENGTH_LONG).show()
        }
        else{
            //update ui
            mAdapter = PlaylistAdapter(this.context,R.layout.playlist_fragment,currentPlaylist!!)
            retInf.findViewById<ListView>(R.id.play_list_view).adapter = mAdapter

        }

        val playlist = retInf.findViewById<ListView>(R.id.play_list_view)
        playlist.setOnItemLongClickListener { parent, view, position, id ->
            deleteItem(position)
        }
        return retInf
    }

    override fun onResume() {
        super.onResume()
        //update ui if there is (probably) a change in data base
        if(shouldUpdateList){
            currentPlaylist = viewModel.loadPlaylist()
            if(currentPlaylist==null){
                Toast.makeText(this.context, "playlist is empty", Toast.LENGTH_LONG).show()
            }
            else{
                //update ui
                mAdapter = PlaylistAdapter(this.context,R.layout.playlist_fragment,currentPlaylist!!)
                play_list_view.adapter = mAdapter
            }
        }
        shouldUpdateList = false
    }

    private fun deleteItem(position:Int) : Boolean{

        var temp = currentPlaylist

        temp!!.removeAt(position)

        viewModel.deleteFromPlaylist(parentContext)

        for(track in temp){
            viewModel.addToPlaylist(track,parentContext)
        }
        //reload playlist
       currentPlaylist = viewModel.loadPlaylist()

        if(currentPlaylist==null){
            Toast.makeText(this.context, "playlist is empty", Toast.LENGTH_LONG).show()
        }
        else{
            //update ui
            mAdapter = PlaylistAdapter(this.context,R.layout.playlist_fragment,currentPlaylist!!)
            play_list_view.adapter = mAdapter
        }
        return true
    }

    companion object {
        var shouldUpdateList = false
    }
}