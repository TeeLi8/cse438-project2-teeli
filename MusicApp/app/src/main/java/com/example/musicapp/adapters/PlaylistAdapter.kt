package com.example.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.musicapp.R
import com.example.musicapp.models.Track

class PlaylistAdapter(context: Context?, id:Int, playList: ArrayList<Track>) : ArrayAdapter<Track>(context,id,playList) {

    //adapter for playlist items

    private val currentPlaylist = playList
    private val currentContext = context

    override fun getCount(): Int {
        return currentPlaylist.size
    }

    override fun getItem(position: Int): Track? {
        return currentPlaylist[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val retInf = LayoutInflater.from(currentContext).inflate(R.layout.playlist_item,parent,false)

        val itemTitle = retInf.findViewById<TextView>(R.id.playlist_item_title)

        itemTitle.text = currentPlaylist[position].title

        val itemInfo = retInf.findViewById<TextView>(R.id.playlist_item_info)
        itemInfo.text = currentPlaylist[position].artist

        return retInf
    }

}