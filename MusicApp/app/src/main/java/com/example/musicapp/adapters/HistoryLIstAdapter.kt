package com.example.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.musicapp.R
import org.w3c.dom.Text

class HistoryLIstAdapter(context: Context?, id:Int, historyList: ArrayList<String>) : ArrayAdapter<String>(context,id,historyList){

    //adapter for ListView in History activity

    private val hl = historyList
    private val parentContext = context

    override fun getCount(): Int {

        return hl.size
    }

    override fun getItem(position: Int): String? {
        return hl[hl.size-1-position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val retInf = LayoutInflater.from(parentContext).inflate(R.layout.history_item,parent,false)

        var name = retInf.findViewById<TextView>(R.id.history_name)
        name.text = hl[hl.size-position-1]  //display in reversed order

        return retInf
    }
}