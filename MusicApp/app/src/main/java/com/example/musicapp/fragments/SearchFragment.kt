package com.example.musicapp.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.musicapp.activities.InfoActivity
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.models.Track
import com.example.musicapp.viewModels.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.search_fragment.*
import kotlinx.android.synthetic.main.track_item.view.*

@SuppressLint("ValidFragment")
class SearchFragment(context: Context) : Fragment(){

    var trackList = ArrayList<Track>()

    val parentContext = context

    private lateinit var viewModel : TrackViewModel

 //   private lateinit var grids : RecyclerView

    private var tAdapter = TrackAdapter()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val retView = inflater.inflate(R.layout.search_fragment,container,false)

        val button = retView.findViewById<Button>(R.id.search_button)
        button.setOnClickListener { search() }

        val recButton = retView.findViewById<Button>(R.id.get_similar_button)
        recButton.setOnClickListener { getSimilar() }

        val topTrack = retView.findViewById<TextView>(R.id.grid_type)
        topTrack.text = "Top Tracks"


        return retView

    }

    override fun onStart() {
        super.onStart()
        search_result_gridview.layoutManager = GridLayoutManager(this.context,2)
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)


        val observer = Observer<java.util.ArrayList<Track>> {
            search_result_gridview.adapter = tAdapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    //return trackList[p0].title == trackList[p1].title
                    return false
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(tAdapter)
            trackList = it ?: ArrayList()
        }

        viewModel.findTopTracks().observe(this, observer)


    }

    //perform a search when history activity is returned by a user click
    //onResume is called after onActivityResult, so main activity can pass a string
    //to the fragment with a static variable
    override fun onResume() {
        super.onResume()
        if(doUpdate){
            doSearch(searchString)
            doUpdate = false
        }
    }

    //search helper
    //the observer code is from the studio
    private fun doSearch(artistName : String){
        search_box.text!!.clear()
        grid_type.text = "Result for: $artistName"

        val observer = Observer<java.util.ArrayList<Track>> {
            search_result_gridview.adapter = tAdapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0].title == trackList[p1].title
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(tAdapter)
            trackList = it ?: ArrayList()
        }

        viewModel.findTrackByArtist(artistName).observe(this, observer)


    }



    private fun search(){
        val artistName = search_box.text.toString()
        if(artistName==""){
            Toast.makeText(this.context,"Please Enter an Artist's name",Toast.LENGTH_SHORT).show()
        }
        else{

              doSearch(artistName)
              viewModel.addToHistory(artistName,parentContext)


        }
    }


    //find similar tracks base on the first track in the playlist
    private fun getSimilar(){
        val playlist = viewModel.loadPlaylist()
        if(playlist!=null&&playlist.size>0){
            val track1 = playlist[0]

            val observer = Observer<java.util.ArrayList<Track>> {
                search_result_gridview.adapter = tAdapter
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                        return trackList[p0].title == trackList[p1].title
                    }

                    override fun getOldListSize(): Int {
                        return trackList.size
                    }

                    override fun getNewListSize(): Int {
                        if (it == null) {
                            return 0
                        }
                        return it.size
                    }

                    override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                        return trackList[p0] == trackList[p1]
                    }
                })
                result.dispatchUpdatesTo(tAdapter)
                trackList = it ?: ArrayList()
            }
            viewModel.findSimilarTrack(track1.title!!,track1.artist!!).observe(this, observer)


        }
        else{
            Toast.makeText(parentContext,"Please add something to playlist first",Toast.LENGTH_LONG).show()
        }
    }

    inner class TrackAdapter : RecyclerView.Adapter<TrackAdapter.NewTrackViewHolder>() {


        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewTrackViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.track_item, p0, false)
            return NewTrackViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: NewTrackViewHolder, p1: Int) {
            val track = trackList[p1]

            p0.trackTitle.text = track.title

            Picasso.with(this@SearchFragment.parentContext).load(track.cURL).into(p0.trackImg)

            p0.row.setOnClickListener {
                val intent = Intent(this@SearchFragment.parentContext, InfoActivity::class.java)
                intent.putExtra(MainActivity.track_title, track.title)
                intent.putExtra(MainActivity.track_cURL, track.cURL)
                intent.putExtra(MainActivity.track_artist, track.artist)
                intent.putExtra(MainActivity.track_playCount, track.playCount)
                intent.putExtra(MainActivity.track_listeners, track.listeners)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return trackList.size
        }

        inner class NewTrackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var trackTitle : TextView = itemView.track_title
            var trackImg: ImageView = itemView.track_cover
        }

    }

    companion object {
        var doUpdate = false
        var searchString = ""
    }



}