package com.example.musicapp.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapters.HistoryLIstAdapter
import com.example.musicapp.viewModels.TrackViewModel
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    private lateinit var viewModel : TrackViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
    }

    override fun onStart() {
        super.onStart()

        history_list.setOnItemClickListener { parent, view, position, id ->
            goBackToMain(history_list.adapter.getItem(position) as String)
        }

        clear_history_button.setOnClickListener {
            clearHistory()
        }

        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        val historyList = viewModel.loadHistory()
        history_list.adapter = HistoryLIstAdapter(this,R.layout.activity_history,historyList)
    }

    //go back to main activity with selected item
    private fun goBackToMain(name:String){
        val intent = Intent(this@HistoryActivity,MainActivity::class.java)
        intent.putExtra(MainActivity.history_name,name)
        setResult(MainActivity.history_back_result,intent)
        finish()
    }

    private fun clearHistory(){
        viewModel.deleteHistory(this@HistoryActivity)

        //update ui ith empty list
        history_list.adapter = HistoryLIstAdapter(this,R.layout.activity_history,ArrayList())
    }

}
