package com.example.musicapp.db

import android.provider.BaseColumns

class DBSettings {

    companion object {
        const val DB_NAME = "music.db"
        const val DB_VERSION = 1
    }

    class DBPLEntry : BaseColumns{   //db for playlist
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_TITLE = "title"
            const val COL_IMAGE = "image"
            const val COL_ARTIST = "artist"
        }
    }

    class History : BaseColumns{     //db for search_history list
        companion object {
            const val TABLE = "historylist"
            const val ID = BaseColumns._ID
            const val COL_NAME = "name"
        }
    }
}