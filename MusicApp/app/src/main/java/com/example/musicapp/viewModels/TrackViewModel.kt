package com.example.musicapp.viewModels

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.AsyncTask
import android.provider.BaseColumns
import android.util.Log
import com.example.musicapp.db.DBSettings
import com.example.musicapp.db.MusicDBHelper
import com.example.musicapp.models.Track
import com.example.musicapp.utils.QueryUtils

class TrackViewModel (application:Application) : AndroidViewModel(application){

    private var musicDBHelper : MusicDBHelper = MusicDBHelper(application)
    private var tracksList: MutableLiveData<ArrayList<Track>> = MutableLiveData()

    private fun loadTracks(query:String){
        TrackAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class TrackAsyncTask : AsyncTask<String,Unit,ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchTrackData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Track>?) {
            if(result==null){
                Log.e("Result","No Result")
            }
            else{
                tracksList.value = result
            }
        }
    }

    //API Call : Last.fm chart.gettoptracks used
    fun findTopTracks():MutableLiveData<ArrayList<Track>>{
        loadTracks("?method=chart.gettoptracks&api_key=$myAPIKey&format=json")
        return tracksList
    }

    //API Call : Last.fm artist.gettoptracks used
    fun findTrackByArtist(artistName:String):MutableLiveData<ArrayList<Track>>{
        loadTracks("?method=artist.gettoptracks&artist=$artistName&api_key=$myAPIKey&format=json")
        return tracksList
    }

    //API Call : Last.fm track.getsimilar used
    fun findSimilarTrack(trackTitle:String,trackArtist:String):MutableLiveData<ArrayList<Track>>{
        loadTracks("?method=track.getsimilar&artist=$trackArtist&track=$trackTitle&api_key=$myAPIKey&format=json")
        return tracksList
    }


    fun loadPlaylist():ArrayList<Track>?{

        val db = musicDBHelper.readableDatabase

        val projection = arrayOf(BaseColumns._ID,DBSettings.DBPLEntry.COL_TITLE,DBSettings.DBPLEntry.COL_IMAGE,DBSettings.DBPLEntry.COL_ARTIST)

        val cursor = db.query(
                DBSettings.DBPLEntry.TABLE,
                projection,
                null,
                null,
                null,
                null,
                null
        )

        var playlist = ArrayList<Track>()
        with(cursor){
            while (moveToNext()){
                val title = getString(getColumnIndexOrThrow(DBSettings.DBPLEntry.COL_TITLE))
                val cover = getString(getColumnIndexOrThrow(DBSettings.DBPLEntry.COL_IMAGE))
                val artist = getString(getColumnIndexOrThrow(DBSettings.DBPLEntry.COL_ARTIST))
                playlist.add(Track(title,cover,artist))
            }
        }

        return playlist
    }

    fun addToPlaylist(track:Track,context: Context){
        val dbHelper = MusicDBHelper(context)
        dbHelper.addTrack(track)

    }

    fun deleteFromPlaylist(context:Context){
        val dbHelper = MusicDBHelper(context)
   //     dbHelper.deleteTrack(rowId.toString())
        dbHelper.deleteTrack()
    }

    fun loadHistory() : ArrayList<String>{

        val db = musicDBHelper.readableDatabase

        val projection = arrayOf(BaseColumns._ID,DBSettings.History.COL_NAME)

        val cursor = db.query(
            DBSettings.History.TABLE,
            projection,
            null,
            null,
            null,
            null,
            null
        )

        var historyList = ArrayList<String>()
        with(cursor){
            while (moveToNext()){
                val name = getString(getColumnIndexOrThrow(DBSettings.History.COL_NAME))
                historyList.add(name)
            }
        }

        return historyList
    }

    fun addToHistory(name:String,context: Context){
        val dbHelper = MusicDBHelper(context)
        dbHelper.addHistory(name)
    }

    fun deleteHistory(context:Context){
        val dbHelper = MusicDBHelper(context)
        dbHelper.deleteHistory()
    }

    companion object {
        const val myAPIKey = "997b2f51b10308d7e807948a0227d9db"
    }
}