package com.example.musicapp.utils

import android.text.TextUtils
import android.util.Log
import com.example.musicapp.models.Track
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset

//QueryUtils: Syntax is from the studios
class QueryUtils {
        companion object {
            private val LogTag = this::class.java.simpleName
            private const val BaseURL = "http://ws.audioscrobbler.com/2.0/" // localhost URL

            fun fetchTrackData(jsonQueryString: String): ArrayList<Track>? {
                val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

                var jsonResponse: String? = null
                try {
                    jsonResponse = makeHttpRequest(url)
                } catch (e: IOException) {
                    Log.e(this.LogTag, "Problem making the HTTP request.", e)
                }

                return extractDataFromJson(jsonResponse)
            }

            private fun createUrl(stringUrl: String): URL? {
                var url: URL? = null
                try {
                    url = URL(stringUrl)
                } catch (e: MalformedURLException) {
                    Log.e(this.LogTag, "Problem building the URL.", e)
                }

                return url
            }

            private fun makeHttpRequest(url: URL?): String {
                var jsonResponse = ""

                if (url == null) {
                    return jsonResponse
                }

                var urlConnection: HttpURLConnection? = null
                var inputStream: InputStream? = null
                try {
                    urlConnection = url.openConnection() as HttpURLConnection
                    urlConnection.readTimeout = 10000 // 10 seconds
                    urlConnection.connectTimeout = 15000 // 15 seconds
                    urlConnection.requestMethod = "GET"
                    urlConnection.connect()

                    if (urlConnection.responseCode == 200) {
                        inputStream = urlConnection.inputStream
                        jsonResponse = readFromStream(inputStream)
                    } else {
                        Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                    }
                } catch (e: IOException) {
                    Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
                } finally {
                    urlConnection?.disconnect()
                    inputStream?.close()
                }

                return jsonResponse
            }

            private fun readFromStream(inputStream: InputStream?): String {
                val output = StringBuilder()
                if (inputStream != null) {
                    val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                    val reader = BufferedReader(inputStreamReader)
                    var line = reader.readLine()
                    while (line != null) {
                        output.append(line)
                        line = reader.readLine()
                    }
                }

                return output.toString()
            }

            private fun extractDataFromJson(trackJson: String?): ArrayList<Track>? {
                if (TextUtils.isEmpty(trackJson)) {
                    return null
                }

                val trackList = ArrayList<Track>()
                try {
                    val baseJsonResponse = JSONObject(trackJson)
                    var trackObject = returnValueOrDefault<JSONObject>(baseJsonResponse,"tracks") as JSONObject?
                    if(trackObject == null){
                        trackObject = returnValueOrDefault<JSONObject>(baseJsonResponse,"toptracks") as JSONObject?

                        if(trackObject==null){
                            trackObject = returnValueOrDefault<JSONObject>(baseJsonResponse,"similartracks") as JSONObject?
                        }
                    }
                    if(trackObject!=null){
                        val trackResults = returnValueOrDefault<JSONArray>(trackObject,"track") as JSONArray?
                        if(trackResults!=null){
                            for(i in 0 until trackResults.length()){

                                //title
                                val title = returnValueOrDefault<String>(trackResults[i] as JSONObject, "name") as String?

                                //cover
                                val covers = returnValueOrDefault<JSONArray>(trackResults[i] as JSONObject,"image") as JSONArray?
                                var cover :String? = null
                                if(covers!=null && covers.length()>=2){
                                    cover = returnValueOrDefault<String>(covers[1] as JSONObject, "#text") as String?
                                }

                                //artist
                                val artist = returnValueOrDefault<JSONObject>(trackResults[i] as JSONObject,"artist") as JSONObject?
                                var artistName : String? = null
                                if(artist!=null){
                                    artistName = returnValueOrDefault<String>(artist,"name") as String?
                                }

                                //playcount
                                val playcount = returnValueOrDefault<String>(trackResults[i] as JSONObject,"playcount") as String?

                                //listeners
                                val listeners = returnValueOrDefault<String>(trackResults[i] as JSONObject,"listeners") as String?


                                if(title!=null&&cover!=null&&artistName!=null){
                                    val newTrack = Track(title,cover,artistName,playcount,listeners)
                                    trackList.add(newTrack)
                                }
                                else{
                                    Log.e("debug","Problem in tca")
                                }

                            }
                        }
                        else{
                            Log.e("debug","cant find tracklist")
                        }

                    }


                } catch (e: JSONException) {
                    Log.e(this.LogTag, "Problem parsing the product JSON results", e)
                }

                return trackList
            }

            private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
                when (T::class) {
                    String::class -> {
                        return if (json.has(key)) {
                            json.getString(key)
                        } else {
                            ""
                        }
                    }
                    Int::class -> {
                        return if (json.has(key)) {
                            json.getInt(key)
                        } else {
                            return -1
                        }
                    }
                    Double::class -> {
                        return if (json.has(key)) {
                            json.getDouble(key)
                        } else {
                            return -1.0
                        }
                    }
                    Long::class -> {
                        return if (json.has(key)) {
                            json.getLong(key)
                        } else {
                            return (-1).toLong()
                        }
                    }
                    JSONObject::class -> {
                        return if (json.has(key)) {
                            json.getJSONObject(key)
                        } else {
                            return null
                        }
                    }
                    JSONArray::class -> {
                        return if (json.has(key)) {
                            json.getJSONArray(key)
                        } else {
                            return null
                        }
                    }
                    else -> {
                        return null
                    }
                }
            }
        }
}