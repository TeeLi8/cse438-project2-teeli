package com.example.musicapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.Menu
import android.view.MenuItem
import com.example.musicapp.activities.HistoryActivity
import com.example.musicapp.adapters.HistoryLIstAdapter
import com.example.musicapp.fragments.PlaylistFragment
import com.example.musicapp.fragments.SearchFragment

import kotlinx.android.synthetic.main.activity_main.*
import javax.xml.transform.Result

data class Section(val tabTitle:String, val fragment: Fragment)

class MainActivity : AppCompatActivity() {


    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var mSectionList = ArrayList<Section>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        //sections
        val searchSection = Section("Search",SearchFragment(this@MainActivity))
        val playlistSection = Section("Playlist", PlaylistFragment(this@MainActivity))
        mSectionList.add(searchSection)
        mSectionList.add(playlistSection)

        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the two
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        val myTabs = findViewById<TabLayout>(R.id.tab)
        myTabs.setupWithViewPager(container)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_history) {
            val intent = Intent(this@MainActivity,HistoryActivity::class.java)
            startActivityForResult(intent, history_code)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == history_code){
            if(resultCode == history_back_result){  //this is only true when history activity is returned because user pressed an item
                val name = data!!.getStringExtra(history_name)
                SearchFragment.doUpdate = true   //tell search fragment to perform a search
                SearchFragment.searchString = name
            }
        }
    }



    //adapter for tabs
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {


        override fun getItem(position: Int): Fragment{
            return mSectionList[position].fragment
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mSectionList[position].tabTitle
        }

        override fun getCount(): Int {
            return mSectionList.size
        }
    }

    companion object {
        const val info_code = 1
        const val history_code = 2
        const val track_title  = "com.example.musicapp.track_name"
        const val track_cURL = "com.example.musicapp.track_curl"
        const val track_artist = "com.example.musicapp.track_artist"
        const val track_playCount = "com.example.musicapp.track_playcount"
        const val track_listeners = "com.example.musicapp.track_listeners"
        const val history_name = "com.example.musicapp.history_name"
        const val history_back_result = 666
    }

}
